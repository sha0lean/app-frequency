<?php

namespace App\Repository;

use App\Entity\User;
use App\Entity\Substyle;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Substyle|null find($id, $lockMode = null, $lockVersion = null)
 * @method Substyle|null findOneBy(array $criteria, array $orderBy = null)
 * @method Substyle[]    findAll()
 * @method Substyle[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SubstyleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Substyle::class);
    }

    // /**
    //  * @return Substyle[] Returns an array of Substyle objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Substyle
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
