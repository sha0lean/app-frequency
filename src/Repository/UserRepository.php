<?php

namespace App\Repository;

use App\Entity\User;
use App\Entity\Style;
use App\Entity\Substyle;
use App\Entity\Song;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function findByStyle(Style $style){

        return $this->createQueryBuilder('User')

        ->join('user.substyles','substyle')
        ->join('substyle.style','style')
        ->andWhere('style = :currentStyle')->setParameter('currentStyle', $style)

        ->setMaxresults(2)
        
        ->getQuery()->getResult();

    }
    public function FindTwoArtist($user)
    {
        $entityManager = $this -> getEntityManager();
        $query=$entityManager->createQuery(
            'SELECT '
        );
    }

// ------------------------------------------------------------------------------------------


public function findBySong(Song $song){
    return $this->createQueryBuilder('user')
    
    ->join('user.song','song')
    ->andWhere('song = :currentSong')->setParameter('currentSong', $song)
    
    ->setMaxresults(2)
    
    ->getQuery()->getResult();
}

// ------------------------------------------------------------------------------------------

public function findBySubstyle(Substyle $substyle){
    return $this->createQueryBuilder('user')

    ->join('user.substyles','substyle')
    ->andWhere('substyle = :currentStyle')->setParameter('currentStyle', $substyle)

    ->setMaxresults(2)

    ->getQuery()->getResult();
}

// /**
    //  * @return User[] Returns an array of User objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?User
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
