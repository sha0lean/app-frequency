<?php

namespace App\Entity;

use App\Entity\Substyle;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\StyleRepository")
 */
class Style
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Substyle", mappedBy="style")
     */
    private $substyles;

    public function __construct()
    {
        $this->substyles = new ArrayCollection();
    }
    
    public function __toString() {
        return $this->name;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection|Substyle[]
     */
    public function getSubstyles(): Collection
    {
        return $this->substyles;
    }

    public function addSubstyle(Substyle $substyle): self
    {
        if (!$this->substyles->contains($substyle)) {
            $this->substyles[] = $substyle;
            $substyle->setStyle($this);
        }

        return $this;
    }

    public function removeSubstyle(Substyle $substyle): self
    {
        if ($this->substyles->contains($substyle)) {
            $this->substyles->removeElement($substyle);
            // set the owning side to null (unless already changed)
            if ($substyle->getStyle() === $this) {
                $substyle->setStyle(null);
            }
        }

        return $this;
    }
}
