<?php

namespace App\Form;

use App\Entity\User;
use App\Entity\Substyle;
use App\Entity\Song;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email')
            ->add('pseudo')
            ->add('firstname')
            ->add('name')
            ->add('birthdate')
            ->add('startyear', DateType::class, [
                'widget' => 'choice',
            ])
            ->add('description')
            ->add('profilepicture')
            ->add('videourl')
            ->add('cover')
            ->add('songs', ChoiceType::class)
            ->add('country', ChoiceType::class, [
                'choices' => [
                    'Europe'         => 'EU',
                    'Germany'        => 'DE',
                    'United Kingdom' => 'UK',
                    'Netherland'     => 'NL',
                    'Norway'         => 'NO',
                    'South Africa'   => 'SA',
                    'Spain'          => 'ES',
                    'Suede'          => 'SE',
                    'Canada'         => 'CA',
                    'France'         => 'FR',
                    'United States'  => 'US',
                    'Swiss'          => 'CH',
                    'Japan'         => 'JA',
                    'Porto Rico'    => 'PR',
                    'Italie'        => 'IT',
                    'Chilie'        => 'CL',
                    'Australie'     => 'AUS',
                    'Illinois'      => 'IL',

                ],
            ])
            ->add('substyles', null, ["expanded" => true]);
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
