<?php

namespace App\DataFixtures;
use App\Entity\Live;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class LiveFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $infos_lives = [
            
            ["Live Tech",      "https://www.twitch.tv/sardoche"],
            ["Live Acid",      "https://www.twitch.tv/sardoche"],
            ["Live House",     "https://www.twitch.tv/sardoche"],
            ["Live Drumnbass", "https://www.twitch.tv/sardoche"],

        ];

        foreach ($infos_lives as $infos_live){
   
            $live = new Live();

            $live
                    ->setName($infos_live[0])
                    ->setLiveurl($infos_live[1]);

            $manager->persist($live); // "commit"
        }  
        $manager->flush(); // "push" to db
    }
}
