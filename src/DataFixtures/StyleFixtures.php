<?php

namespace App\DataFixtures;

use App\Entity\Style;
use App\Entity\Substyle;
use App\Repository\UserRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class StyleFixtures extends Fixture implements DependentFixtureInterface
{
    private $userRepo;
    public function __construct(UserRepository $userRepo)
    {
        $this->userRepo = $userRepo;
    }
    public function load(ObjectManager $manager)
    {
        
        $genres = [ 
            ["House",     ["Acid House","Afro House","Big Room House","Chicago House", "Electro House", "Hard House", "Jazztronica", "Kwaito House", "Latin House"]],    
            ["Techno",    ["Acid Techno","Detroit Techno","Hardtek","Industrial Techno" , "Minimal Techno", "Tech House", "Trance Techno"]],    
            ["Garage",    ["UK Bass","Future Garage","UK Funky","Grime", "UK Garage"]],    
            ["DrumnBass", ["Jump up","Deep/Techstep","Jungle/Ragga","DnB","Halftime"]], 
            ["Dubstep",   ["Deep/Dark Dubstep","Dubstep","Purple Music","Drumstep","Riddim"]],    
        ];
        
        // pour chaque $genre du [$genres]
        foreach ($genres as $genre){
            $style = new Style(); // on lui instancit l'objet Style()

            $style->setName($genre[0])   // on attribut "Name" au big genre 
                  ->setDescription("Vivamus quis mauris enim. In eget convallis libero, vitae blandit eros. Vestibulum tristique maximus odio, at semper dui facilisis quis. Vestibulum molestie mollis ex sed suscipit. ");
        
            // pour chaque sous-genre du deuxieme tableau de [$genres]
            foreach ($genre[1] as $substyle){
                $mysub = new Substyle(); // on lui instancit l'objet Substyle()
                $mysub->setStyle($style)
                      ->setName($substyle)
                      ->setDescription("la sous-description");
                
                $users = $this->userRepo->findAll();
                shuffle($users);
                $users = array_slice($users, 0, rand(1, count($users)-1));
                
                foreach ($users as $user){
                    $mysub->addUser($user);
                }

                $manager->persist($mysub);
            }
            $manager->persist($style); // "commit un style"
        }   
        
        $manager->flush(); // "push" to db
    }
    public function getDependencies()
    {
        return [
            UserFixtures::class
        ];
    }
}