<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class UserFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < 10; $i++) 
        {
            $user = new User();
            //$random_user = openssl_random_pseudo_bytes ( 10 );
            
            //$user->setPseudo($i.$i.$i .random_bytes (5));
            $user->setPseudo($i.str_shuffle ("CREA"));

            $user->setFirstname("Remy");
            $user->setName("LGD");
            $user->setBirthdate(new \DateTime("30-06-1995"));
            $user->setStartyear(new \DateTime("14-02-2010"));
            $user->setDescription("Lorem ipsum dolor .");
            $user->setCountry("DE");
            $user->setEMail("user$i@domain.com");
            $user->setRoles(["ROLE_USER"]);
            $user->setPassword("$i$i$i$i$i$i");
            $user->setProfilepicture("https://www.croquetteland.com/wp/wp-content/uploads/2019/08/chat_urine_partout-1620x1080.jpg");
            $user->setVideourl("https://www.youtube.com/watch?v=WFN9F2FtGCg");

            $manager->persist($user);    // "commit"
        }

        $manager->flush();
    }
}
