<?php

namespace App\Controller;
use App\Entity\Live;
use App\Repository\LiveRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index()
    {
        return $this->render('home/index.html.twig', [
        ]);
    }
    /**
     * @Route("/search", name="search", methods={"GET"})
     */
    public function search()
    {
        return $this->render('home/search.html.twig', [
        ]);
    }
    /**
     * @Route("/live", name="live", methods={"GET"})
     */
    public function live(LiveRepository $liveRepository)
    {
        return $this->render('home/live.html.twig', [
            // 'live' => $live,
            'lives' => $liveRepository->findAll(),
        ]);
    }
}
