<?php

namespace App\Controller;

use App\Entity\Song;
use App\Entity\Style;
use App\Entity\Substyle;
use App\Entity\User;
use App\Form\SubstyleType;
use App\Repository\SubstyleRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/substyle")
 */
class SubstyleController extends AbstractController
{
    /**
     * @Route("/", name="substyle_index", methods={"GET"})
     */
    public function index(SubstyleRepository $substyleRepository): Response
    {
        return $this->render('substyle/index.html.twig', [
            'substyles' => $substyleRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="substyle_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $substyle = new Substyle();
        $form = $this->createForm(SubstyleType::class, $substyle);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($substyle);
            $entityManager->flush();

            return $this->redirectToRoute('substyle_index');
        }

        return $this->render('substyle/new.html.twig', [
            'substyle' => $substyle,
            'form' => $form->createView(),
        ]);
    }

   /**
    * @Route("/{id}", name="substyle_show", methods={"GET"}, requirements={"id"="\d+"})
    */
   public function show(Substyle $substyle, User $user)
   {
      
       return $this->render('substyle/subshow.html.twig', [
           'substyle' => $substyle,
           'user' => $user,
       ]);
   }
    /**
    * @Route("/{name}/{id}", name="substyle_show_view", methods={"GET"}, requirements={"id"="\d+"})
    */
    public function showSubs(Substyle $substyle,User $user,Song $song, Style $style): Response
    {
        return $this->render('style/subshow.html.twig', [
            'substyle' => $substyle,
            'user'=> $user,
            'song'=> $song,
            'style' => $style,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="substyle_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Substyle $substyle): Response
    {
        $form = $this->createForm(SubstyleType::class, $substyle);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('substyle_index');
        }

        return $this->render('substyle/edit.html.twig', [
            'substyle' => $substyle,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="substyle_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Substyle $substyle): Response
    {
        if ($this->isCsrfTokenValid('delete'.$substyle->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($substyle);
            $entityManager->flush();
        }

        return $this->redirectToRoute('substyle_index');
    }
}
